package de.bloosberg.basti.childresuscalc;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.round;

public class MainActivity extends AppCompatActivity {

    EditText ageEditText;
    EditText weightEditText;

    Integer age;
    Integer weight;

    ListView resultsListView;

    public void clearButtonClicked(View view){

        Log.i("Info", "Clear Button clicked");

        // Clear both age and weight EditTexts
        ageEditText.setText("");
        weightEditText.setText("");

        // Delete list of Drugs/Devices
        resultsListView.setAdapter(null);

    }


    public static boolean isInteger(String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),10) < 0) return false;
        }
        return true;
    }


    public static boolean isOneToTwelve(Integer i) {
        if (i > 0 && i < 13) return true;
        else return false;
    }


    public void displayErrorAge() {
        String errorMessage = "Enter age as a whole number 1-12 years";
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
    }


    public static Integer weightAPLS(Integer age) {
        Integer weightGuessed;
        if (age >= 1 && age <= 5) weightGuessed = (age *2) +8;
        else weightGuessed = (age * 3) +7;
        return weightGuessed;
    }


    public void displayErrorWeight() {
        String errorMessage = "Enter weight as a whole number";
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
    }


    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    public String getTubeSize(Integer age) {
        String tubeSize;
        Double ts;
        if (age <= 2)  {
            tubeSize = "4.0 - 4.5";
        // if age/4+4 is sth.25 or sth.75 return next smaller to next larger size
        } else if (age % 4 == 3 || age % 4 == 1) {
            ts = ((((double) (age)) / 4) + 4) - 0.25;
            tubeSize = ts + " - " + (ts + 0.5);
        } else {
            ts = ((((double) (age)) / 4) + 4) ;
            tubeSize = ts.toString();
        }
        return tubeSize;
    }


//    public ArrayList<String> calcDoses(Integer age, Integer weight) {
//
//        Integer defi = weight * 4;
//        Double adr = round((weight * 0.01),2);
//        Integer amio = weight * 5;
//        String tubeSize = getTubeSize(age);
//        Integer bolus = weight * 20;
//        Integer cardiov1 = weight;
//        Integer cardiov2 = weight * 2;
//        Double eto = round((weight * 0.3),2);
//        Double mida1 = round((weight * 0.15),2);
//        Double mida2 = round((weight * 0.3),2);
//        Integer thio1 = weight * 3;
//        Integer thio2 = weight * 5;
//        Double esketam1 = round((weight * 0.5), 2);
//        Double esketam2 = round((weight * 1.0), 2);
//        Integer ketam1 = weight * 1;
//        Integer ketam2 = weight * 2;
//        Double morph = round((weight * 0.1), 2);
//        Double fenta1 = round((weight * 0.001), 3);
//        Double fenta2 = round((weight * 0.003), 3);
//        Double succi = round((weight * 1.5), 2);
//        Integer rocu = weight;
//
//
//        final ArrayList<String> listItems = new ArrayList<String>(Arrays.asList(
//                "Defibrillation: " + defi + " Joules",
//                "Adrenaline: " + adr + " mg",
//                "Amiodarone: " + amio + " mg",
//                "Tracheal Tube ID: " + tubeSize + " mm",
//                "IV fluid bolus: " + bolus + " ml",
//                "Cardioversion: " + cardiov1 + " (2nd: " + cardiov2 + ") Joules",
//                "Etomidate: " + eto + " mg",
//                "Midazolam: " + mida1 + " - " + mida2 + " mg",
//                "Thiopental: " + thio1 + " - " + thio2 + " mg",
//                "Esketamin: " + esketam1 + " - " + esketam2 + " mg",
//                "Ketamin: " + ketam1 + " - " + ketam2 + " mg",
//                "Morphine: " + morph + " mg",
//                "Fentanyl: " + fenta1 + " - " + fenta2 + " mg",
//                "Succinylcholine: " + succi + " mg",
//                "Rocuronium: " + rocu + " mg"
//        ));
//
//        return listItems;
//    }


    private ArrayList<Map<String, String>> buildData() {

        Integer defi = weight * 4;
        Double adr = round((weight * 0.01),2);
        Integer amio = weight * 5;
        String tubeSize = getTubeSize(age);
        Integer bolus = weight * 20;
        Integer cardiov1 = weight;
        Integer cardiov2 = weight * 2;
        Double eto1 = round((weight * 0.2),2);
        Double eto2 = round((weight * 0.3),2);
        Double mida1 = round((weight * 0.1),2);
        Double mida2 = round((weight * 0.2),2);
        Integer thio1 = weight * 3;
        Integer thio2 = weight * 5;
        Double esketam1 = round((weight * 0.5), 2);
        Double esketam2 = round((weight * 1.0), 2);
        Integer ketam1 = weight * 1;
        Integer ketam2 = weight * 2;
        Double morph = round((weight * 0.1), 2);
        Double fenta1 = round((weight * 0.001), 3);
        Double fenta2 = round((weight * 0.003), 3);
        Double succi = round((weight * 1.5), 2);
        Integer rocu = weight;


        ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
        list.add(putData("Defibrillation: " + defi + " Joules", "4 Joules per kg/BW"));
        list.add(putData("Adrenaline: " + adr + " mg", "0.01 mg per kg/BW"));
        list.add(putData("Amiodarone: " + amio + " mg", "5 mg per kg/BW"));
        list.add(putData("Tracheal Tube ID: " + tubeSize + " mm", "Age/4+4 (0.5 mm less if cuffed)"));
        list.add(putData("IV fluid bolus: " + bolus + " ml", "20 ml per kg/BW"));
        list.add(putData("Cardioversion: " + cardiov1 + " (2nd: " + cardiov2 + ") Joules", "1 Joule (2nd: 2 Joules) per kg/BW"));
        list.add(putData("Etomidate: " + eto1 + " - " + eto2 + " mg", "0.2-0.3 mg per kg/BW"));
        list.add(putData("Midazolam: " + mida1 + " - " + mida2 + " mg", "0.1-0.2 mg per kg/BW"));
        list.add(putData("Thiopental: " + thio1 + " - " + thio2 + " mg", "3-5 mg per kg/BW"));
        list.add(putData("Esketamin: " + esketam1 + " - " + esketam2 + " mg", "0.5-1 mg per kg/BW"));
        list.add(putData("Ketamin: " + ketam1 + " - " + ketam2 + " mg", "1-2 mg per kg/BW"));
        list.add(putData("Morphine: " + morph + " mg", "0.1 mg per kg/BW"));
        list.add(putData("Fentanyl: " + fenta1 + " - " + fenta2 + " mg", "0.001-0.003 mg per kg/BW"));
        list.add(putData("Succinylcholine: " + succi + " mg", "1.5 mg per kg/BW"));
        list.add(putData("Rocuronium: " + rocu + " mg", "1 mg per kg/BW"));
        return list;
    }

    private HashMap<String, String> putData(String dosage, String formula) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("dosage", dosage);
        item.put("formula", formula);
        return item;
    }


    public void goButtonClicked(View view){

        // Hide keyboard
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        if (isInteger(ageEditText.getText().toString()) && isOneToTwelve(Integer.valueOf(ageEditText.getText().toString()))) {
            age = Integer.valueOf(ageEditText.getText().toString());

            if (weightEditText.getText().toString().isEmpty()) {
                weight = weightAPLS(age);
                weightEditText.setText(weight.toString());
            } else if (isInteger(weightEditText.getText().toString())) {
                weight = Integer.valueOf(weightEditText.getText().toString());
            } else displayErrorWeight();

            // Connect ArrayAdapter to Array with list items from Method calcDoses
//            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.mylistview1, calcDoses(age, weight));
//
//            resultsListView.setAdapter(arrayAdapter);

            ArrayList<Map<String, String>> list = buildData();
            String[] from = { "dosage", "formula" };
//            int[] to = { android.R.id.text1, android.R.id.text2 };
            int[] to = {R.id.text1, R.id.text2};

//            SimpleAdapter adapter = new SimpleAdapter(this, list,
//                    android.R.layout.simple_list_item_2, from, to);
            SimpleAdapter adapter = new SimpleAdapter(this, list, R.layout.mylistview1,from, to);
            resultsListView.setAdapter(adapter);

        } else displayErrorAge();

    }


    public void toInfoActivity(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ageEditText = (EditText) findViewById(R.id.ageEditText);
        weightEditText = (EditText) findViewById(R.id.weightEditText);

        resultsListView = (ListView)findViewById(R.id.resultsListView);


    }

}
